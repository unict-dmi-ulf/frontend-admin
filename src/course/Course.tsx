import React from 'react';
import { useParams } from 'react-router-dom';
import './Course.scss';

interface Params {
  id: string;
}

const Course = (): JSX.Element => {
  const params: Params = useParams();

  console.log(params.id);

  return (
    <div className="Course">
      Corso ID: { params.id }
    </div>
  );
};

export default Course;
