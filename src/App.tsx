import React from 'react';
import {
  Switch,
  Route,
  HashRouter,
  Redirect
} from 'react-router-dom';
import Menu from './menu/Menu';
import Footer from './footer/Footer';
import './App.scss';
import MainContent from './main-content/MainContent';
import Course from './course/Course';

function App() {
  return (
    <div className="App">
      <Menu />
      <br/>
      <div className="container">
        <div className="container-fluid mt-space">
          <HashRouter basename="/">
            <Switch>
              <Route exact path="/home" component={MainContent} />
              <Route exact path="/course/:id" component={Course} />
              <Redirect to="/home" />
            </Switch>
          </HashRouter>
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default App;
