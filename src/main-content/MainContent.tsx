import React, { FunctionComponent } from 'react';
import { ListGroup, ListGroupItem } from 'react-bootstrap';
import './MainContent.scss';
// const { REACT_APP_API_URL } = process.env;

const MainContent: FunctionComponent = () => {
  // fetch(`${REACT_APP_API_URL}/findALlCourse`).then(res => console.log(res));

  // TODO: ottenere i corsi dalle API
  const mockCourses = {
    _embedded: {
      courses: [
        {
          id: 0,
          lectures: [
            {
              date: "29-09-1988",
              id: 0,
              questionnaire: {
                id: 0,
                questions: [
                  {
                    id: 0,
                    text: "SistemiCloud - lezione - 1, com'è andata?",
                    type: "OPEN_RESPONSE"
                  }
                ]
              }
            }
          ],
          name: "SistemiCloud"
        },
        {
          id: 1,
          lectures: [
            {
              date: "29-09-1988",
              id: 0,
              questionnaire: {
                id: 0,
                questions: [
                  {
                    id: 0,
                    text: "SistemiOperativi - lezione - 1, com'è andata?",
                    type: "OPEN_RESPONSE"
                  }
                ]
              }
            }
          ],
          name: "SistemiOperativi"
        }
      ]
    },
    _links: {
      self: {
        href: "http://localhost:9011/courses"
      },
      profile: {
        href: "http://localhost:9011/profile/courses"
      }
    }
  };

  return (
    <div className="MainContent">
      <h2>Corsi</h2>
      <br/>
      <ListGroup>
        {mockCourses._embedded.courses.map((c, index) =>
          <ListGroupItem key={index}>
            {c.name}
          </ListGroupItem>
        )}
        <ListGroupItem>
          <a href="#/course/3">esempio corso id 3</a>
        </ListGroupItem>
      </ListGroup>
    </div>
  );
};

export default MainContent;
